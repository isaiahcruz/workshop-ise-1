import boto3
import os
import json

client = boto3.client('s3')

def create_file(event):
    key = event.get('key')
    body = event.get('body')

    client.put_object(
        Bucket=os.environ["S3_BUCKET"],
        Key=key,
        Body=json.dumps(body)
    )

    response = {
      "status": "success",
      "body": "file created"
    }

    return response
