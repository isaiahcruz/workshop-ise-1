from .services.intro.hello import hello
from .services.intro.name import name
from .services.intro.db import db
from .services.intro.create_file import create_file
from .services.intro.get_file import get_file
from .services.intro.update_file import update_file

actions = {
  "hello": hello,
  "name": name,
  "db": db,
  "create_file": create_file,
  "get_file": get_file,
  "update_file": update_file
}
