from .actions import actions
import traceback

def main(event, context):
  try:
    action = event.get('action')

    if action in actions:
      result = actions[action](event)
      
      return result
    else:
      return {'status': 'error', 'message': 'Action not found'}
  except Exception as error:
    print(traceback.format_exc())
    return {'status': 'error', 'message': 'Something went wrong'}
